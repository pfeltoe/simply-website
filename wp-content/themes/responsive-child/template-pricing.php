<!-- Header -->
<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template Name:  Yak - Pricing Page */
get_header(); ?>


<div class="grid col-940 feat-getearlyaccess">
    <table>
        <thead>
            <tr>
                <th></th>
                <th>
                    <h1>Just Starting</h1>
                </th>
                <th>
                    <h1>Growing Business</h1>
                </th>
                <th>
                    <h1>Lots of Data!</h1>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr class="pricing-row">
                <td></td>
                <td>
                    <h3>Free</h3>
                <td>
                    <h3>$500</h3><p>/month</p>
                <td>
                    <p>Let's talk!</p>
                </td>
            </tr>
            <tr>
                <td>Metrics Dashboard</td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Standard Integrations<span>Google Analytics, Adwords, Facebook, Twitter</span></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Full data audit and implementation</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Data Concierge</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Support</td>
                <td>Online</td>
                <td>Unlimited</td>
                <td>Unlimited</td>
            </tr>
            <tr>
                <td>Dedicated data analyst</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Custom reports</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Custom Integrations</td>
                <td><i class="fa fa-times"></i></td>
                <td>Limited</td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
        </tbody>
    </table>
</div>


<div class="grid col-940 feat-freetrial">
    <a href="#getstarted">Talk to one of our experts</a>
</div>

<div class="grid col-940 feat-faq">
    <section>
        <div class="grid col-940 center">
            <h1>FAQs</h1>
        </div>
        <div class="grid col-460">
            <h4>How are you different from other dashboard tools?</h4>
            <p>Although we do include a dashboard, we’re not a dashboard tool. Our clients often refer to us as their "complete data solution". We analyze all of your data and use predictive and human intelligence to provide you actionable insights on your data.</p>
        </div>
        <div class="grid col-460 fit">
            <h4>Is unlimited support actually unlimited?</h4>
            <p>Yep! Any question or concern you have - give us a call, chat to us in-app, or send us an email. We will get back you within 24 hours (usually less).</p>
        </div>
    </section>
    <section>
        <div class="grid col-460">
            <h4>How do I get setup/implement?</h4>
            <p>If you select either of our paid plans - setup is included. We go through each of your data sources to make sure you are tracking and capturing the right data based on your KPIs.</p>
        </div>
        <div class="grid col-460 fit">
            <h4>What happens if I change my site or marketing plan?</h4>
            <p>If you make any changes to your data structure while we’re working together, we will make sure you’re set up correctly and still tracking all of the right things.</p>
        </div>
    </section>
    <section>
        <div class="grid col-460">
            <h4>Do you work with mobile sites or mobile data?</h4>
            <p>Of course! You can also access your dashboard on your phone or tablet. We believe you should be able to access your data and insights from anywhere at any time.</p>
        </div>
    </section>
</div>

<!-- Footer -->
<?php get_footer(); ?>