<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */

/*
 * Globalize Theme options
 */
 
global $responsive_options;
$responsive_options = responsive_get_options();
?>

        <?php responsive_wrapper_bottom(); // after wrapper content hook ?>
        </div><!-- end of #wrapper -->
    <?php responsive_wrapper_end(); // after wrapper hook ?>
    </div><!-- end of #container -->
<?php responsive_container_end(); // after container hook ?>

<footer>

    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

    <!-- <ul>
        <li>
            <a target="_blank" href="https://twitter.com/datayak">
                <i class="fa fa-lg fa-twitter"></i>
            </a>
        </li>
        <li>
            <a target="_blank" href="https://facebook.com/datayak">
                <i class="fa fa-lg fa-facebook"></i>
            </a>
        </li>
        <li>
            <a target="_blank" href="mailto:info@datayak.co">
                <i class="fa fa-lg fa-envelope"></i>
            </a>
        </li>
    </ul> -->

</footer>

<?php responsive_footer_after(); ?>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/script.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.sticky.js"></script>   
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.remodal.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/validetta/validetta.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/mask/jquery.mask.min.js"></script>
</body>
</html>