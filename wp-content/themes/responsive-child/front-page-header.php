<div id="hero">
	<div>
		<span>Actionable data intelligence for SMBs</span>
		<h1>You have data, get real answers</h1>
		<a href="#getstarted">Talk to a data expert for free</a>

	</div>
</div>

<div class="grid col-940 feat-audit">
	<p>Want better marketing ROI? Get your site analyzed for free.</p>
	<form accept-charset="UTF-8" action="https://formkeep.com/f/d9cbaf9eb143" method="POST" id="form-newsletter">
		<input type="hidden" name="utf8" value="?">
		<input type="email" id="email" name="email" placeholder="audit@me.com" data-validetta="required,email">
		<input type="submit" value="Get Your Free Audit">
	</form>
</div>