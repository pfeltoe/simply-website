jQuery(document).ready(function ($) {
    $("#form-register").validetta();
    $("#form-emailonly").validetta();
    $("#form-newsletter").validetta();
    $("#form-question").validetta();

    $(".feat-audit span").click(function () {
        $(".feat-audit .description").slideToggle("slow", function () { });
    });

    $('a.sign-up').click(function () {
        var pricingplan = $(this).attr('data-pricingplan');
        $('#pricingplan').val(pricingplan);
    });

    function openModal() {
        if (window.location.pathname.indexOf('pricing') > -1) {
            return;
        }

        var inst = $('[data-remodal-id=question]').remodal();
        inst.open();
    }
    timer = setTimeout(openModal, 60000);

    $('.question a').click(function () {
        clearTimeout(timer);
    });

});